package CRUD;

import java.sql.Date;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.*;


public class WorkJDBCTemplate implements WorkDAO {
	 private DataSource dataSource;
	 private JdbcTemplate jdbcTemplateObject;
	 
	 public void setDataSource(DataSource dataSource) {
	      this.dataSource = dataSource;
	      this.jdbcTemplateObject = new JdbcTemplate(dataSource);
	   }
	   public void create(String workname, String startingday, String endingday, String status) {
	      String SQL = "insert into work (workname, startingday, endingday, status) values (?, ?, ?, ?)";
	      jdbcTemplateObject.update( SQL, workname, startingday, endingday, status);
	      System.out.println("Created Record WorkName = " + workname + " Startingday = " + startingday + " endingday=" +endingday + " status:"+status);
	      return;
	   }
	   
	   public List<Work> listWorks() {
	      String SQL = "select * from work";
	      List<Work> works = jdbcTemplateObject.query(SQL, new WorkMapper());
	      return works;
	   }
	   public void delete(String workname) {
	      String SQL = "delete from work where workname = ?";
	      jdbcTemplateObject.update(SQL, workname);
	      System.out.println("Deleted Record where workname = " + workname );
	      return;
	   }
	   public void update(String workname, String status){
	      String SQL = "update work set status = ? where workname = ?";
	      jdbcTemplateObject.update(SQL, status, workname);
	      System.out.println("Updated Record with WorkName : " + status );
	      return;
	   }
	
	 
}
