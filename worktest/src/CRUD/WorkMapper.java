package CRUD;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import java.sql.Date;
import java.sql.SQLTransactionRollbackException;
import java.text.SimpleDateFormat;


public class WorkMapper implements RowMapper<Work> {
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
	
	public Work mapRow(ResultSet rs, int rowNum) throws SQLException {
	      Work work = new Work();
	      work.setWorkname(rs.getString("WORKNAME"));
	      work.setStartingday(rs.getString("STARTINGDAY"));
          work.setEndingday(rs.getString("ENDINGDAY"));
	      work.setStatus(rs.getString("STATUS"));
	      
	      return work;
	   }
}
