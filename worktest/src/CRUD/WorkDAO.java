package CRUD;
import java.sql.Date;
import java.util.List;
import javax.sql.DataSource;


public interface WorkDAO {
	 /** 
     * This is the method to be used to initialize
     * database resources ie. connection.
  */
  public void setDataSource(DataSource ds);
  
  /** 
     * This is the method to be used to create
     * a record in the Student table.
  */
  public void create(String workname, String startingday, String endingday, String status);
  
  
  
  /** 
     * This is the method to be used to list down
     * all the records from the Student table.
  */
  public List<Work> listWorks();
  
  /** 
     * This is the method to be used to delete
     * a record from the Student table corresponding
     * to a passed student id.
  */
  public void delete(String workname);
  
  /** 
     * This is the method to be used to update
     * a record into the Student table.
  */
  public void update(String workname, String status);

}
