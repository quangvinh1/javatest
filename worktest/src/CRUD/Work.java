package CRUD;

import java.util.Date;

public class Work {
	private String workname;
	private String  startingday;
	private String  endingday;
	private String status;
	
	public String getWorkname() {
		return workname;
	}
	public void setWorkname(String workname) {
		this.workname = workname;
	}
	public String getStartingday() {
		return startingday;
	}
	public void setStartingday(String startingday) {
		this.startingday = startingday;
	}
	public String getEndingday() {
		return endingday;
	}
	public void setEndingday(String endingday) {
		this.endingday = endingday;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
