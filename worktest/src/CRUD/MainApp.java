package CRUD;

import java.sql.Date;
import java.util.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import CRUD.WorkJDBCTemplate;



public class MainApp {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");

	      WorkJDBCTemplate workJDBCTemplate = 
	         (WorkJDBCTemplate)context.getBean("workJDBCTemplate");
	      
	      System.out.println("------Add Work Records ----------------------------------------" );
	      workJDBCTemplate.create("create", "2020-02-02","2020-03-02","under");
	      workJDBCTemplate.create("fix","2020-03-02","2020-05-02","finish");
	      workJDBCTemplate.create("driving", "2021-07-02","2021-03-02","ongoing");
	      
	      System.out.println("----Edit Record where Workname = create -------------------------" );
	      workJDBCTemplate.update("create", "Has been edited");
	      
	      System.out.println("----Delete Record where Workname = fix -------------------------" );
	      workJDBCTemplate.delete("fix");

	      System.out.println("------ List of Works--------------------------------------------" );
	      
	      List<Work> works = workJDBCTemplate.listWorks();
	      
	      for (Work record : works) {
	         System.out.print("work name : " +record.getWorkname() );
	         System.out.print(", starting day : " + record.getStartingday() );
	         System.out.print(", ending day : " + record.getEndingday());
	         System.out.println(", status : " + record.getStatus());
	      }
	   }

	}


